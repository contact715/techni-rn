export const routes = {
  LOGIN: "Login",
  LOCAL_TODOS: "LocalTodos",
  TODOS: "Todos",
  DASHBOARD: "Dashboard",
  CHARACTERS: "Characters",
  CHARACTER: "Character",
};
