import { createStackNavigator } from "@react-navigation/stack";

import { LocalTodos } from "../screens/LocalTodos";
import Login from "../screens/Login";
import { Todos } from "../screens/Todos";
import { DashboardScreen } from "../screens/DashboardScreen";
import { CharactersScreen } from "../screens/CharactersScreen";
import { routes } from "../constants/routes";
import { CharacterScreen } from "../screens/CharacterScreen";

const Stack = createStackNavigator();

export const RootStackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName={routes.LOGIN}
      screenOptions={
        {
          // headerShown: false,
        }
      }
    >
      <Stack.Screen name={routes.LOGIN} component={Login} />
      <Stack.Screen
        name={routes.LOCAL_TODOS}
        component={LocalTodos}
        options={
          {
            // headerShown: true,
          }
        }
      />
      <Stack.Screen name={routes.TODOS} component={Todos} />
      <Stack.Screen name={routes.DASHBOARD} component={DashboardScreen} />
      <Stack.Screen name={routes.CHARACTERS} component={CharactersScreen} />
      <Stack.Screen name={routes.CHARACTER} component={CharacterScreen} />
    </Stack.Navigator>
  );
};
