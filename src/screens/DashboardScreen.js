import { View, Text, Button, StyleSheet } from "react-native";
import LottieView from "lottie-react-native";
import animationJson from "../../assets/hamster.json";

import { routes } from "../constants/routes";

export const DashboardScreen = ({ navigation, route }) => {
  const {
    params: { login },
  } = route;

  const goToTodos = () => {
    navigation.navigate(routes.TODOS);
  };

  const goToLocalTodos = () => {
    navigation.navigate(routes.LOCAL_TODOS);
  };

  const goToLogin = () => {
    navigation.navigate(routes.LOGIN);
  };

  const goToCharacters = () => {
    navigation.navigate(routes.CHARACTERS);
  };

  return (
    <View>
      <Text>Dashboard - {login}</Text>
      <LottieView
        autoPlay
        loop
        source={animationJson}
        style={styles.lottieView}
      />
      <Button title="Todos" onPress={goToTodos} />
      <Button title="Local Todos" onPress={goToLocalTodos} />
      <Button title="Login" onPress={goToLogin} />
      <Button title="Characters" onPress={goToCharacters} />
    </View>
  );
};

const styles = StyleSheet.create({
  lottieView: {
    width: "100%",
    height: 300,
  },
});
