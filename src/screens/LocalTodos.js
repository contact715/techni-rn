import "react-native-get-random-values";

import {
  StyleSheet,
  View,
  Text,
  Pressable,
  TextInput,
  FlatList,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect, useState, useCallback } from "react";
import { useForm, Controller } from "react-hook-form";
import { v4 as uuidv4 } from "uuid";
import { routes } from "../constants/routes";

const TODO_KEY = "todoList";

export const LocalTodos = ({ navigation }) => {
  const { control, handleSubmit, reset } = useForm({
    defaultValues: {
      name: "",
    },
  });
  const [todos, setTodos] = useState([]);

  const loadData = useCallback(async () => {
    const response = await AsyncStorage.getItem(TODO_KEY);

    if (response === null) {
      return;
    }
    setTodos(JSON.parse(response));
  }, []);

  const saveTodo = async (newTodo) => {
    await AsyncStorage.setItem(TODO_KEY, JSON.stringify([...todos, newTodo]));
    await loadData();
  };

  const onSubmit = (data) => {
    saveTodo({
      id: uuidv4(),
      ...data,
    });
    reset();
  };

  const resetStorage = () => {
    AsyncStorage.removeItem(TODO_KEY);
  };

  useEffect(() => {
    loadData();
  }, [loadData]);

  return (
    <View>
      <Text>Local Todos</Text>
      <Controller
        name="name"
        control={control}
        render={({ field: { onChange, ...rest } }) => (
          <TextInput
            style={styles.input}
            placeholder="Type your task..."
            onChangeText={onChange}
            {...rest}
          />
        )}
      />
      <View style={styles.buttonsContainer}>
        <ContainedButton label="Reset Store" onPress={resetStorage} />
        <ContainedButton label="Refresh" onPress={loadData} />
        <ContainedButton label="Save" onPress={handleSubmit(onSubmit)} />
      </View>
      <FlatList
        data={todos}
        keyExtractor={(todo) => todo.id}
        renderItem={({ item }) => (
          <View>
            <Text>{item.name}</Text>
          </View>
        )}
      />
      <ContainedButton
        label="Navigate to Login"
        onPress={() => {
          navigation.navigate(routes.LOGIN);
        }}
      />
    </View>
  );
};

const ContainedButton = ({ onPress, label }) => (
  <Pressable style={styles.button} onPress={onPress}>
    <Text style={styles.textButton}>{label}</Text>
  </Pressable>
);

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: "row",
    gap: 16,
    justifyContent: "center",
  },
  button: {
    padding: 16,
    backgroundColor: "green",
    borderRadius: 16,
  },
  textButton: {
    color: "#fff",
  },
  input: {
    padding: 16,
    borderWidth: 1,
    borderColor: "#000",
  },
});
