import { useState, useEffect } from "react";
import { View, Text, ScrollView, FlatList } from "react-native";

const getTodos = async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/todos");
  const data = await response.json();
  return data;
};

export const Todos = () => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    getTodos().then((data) => {
      setTodos(data);
    });
  }, []);

  return (
    <FlatList
      data={todos}
      keyExtractor={(todo) => todo.id}
      renderItem={({ item, index }) => {
        return (
          <Text>
            {index}. {item.title}
          </Text>
        );
      }}
    />
  );

  //   return (
  //     <ScrollView>
  //       <Text>Todos</Text>
  //       {todos.map((todo, index) => (
  //         <View key={todo.id}>
  //           <Text>
  //             {index}. {todo.title}
  //           </Text>
  //         </View>
  //       ))}
  //     </ScrollView>
  //   );
};
