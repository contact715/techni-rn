import { useState, useEffect } from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { routes } from "../constants/routes";

export const CharactersScreen = ({ navigation }) => {
  const [characters, setCharacters] = useState([]);

  const goToCharacter = (id) => {
    navigation.navigate(routes.CHARACTER, {
      id,
    });
  };

  useEffect(() => {
    fetch("https://rickandmortyapi.com/api/character")
      .then((response) => response.json())
      .then((data) => {
        setCharacters(data.results);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  return (
    <View>
      <FlatList
        numColumns={3}
        data={characters}
        renderItem={({ item }) => {
          return (
            <CharacterTile {...item} onPress={() => goToCharacter(item.id)} />
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 100,
    width: 100,
  },
});

const CharacterTile = ({ image, name, onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <Image
      style={styles.image}
      source={{
        uri: image,
      }}
    />
    <Text>{name}</Text>
  </TouchableOpacity>
);
