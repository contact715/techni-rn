import { View, Text } from "react-native";


// https://rickandmortyapi.com/api/character/3

export const CharacterScreen = ({ navigation, route }) => {
  const {
    params: { id },
  } = route;

  return (
    <View>
      <Text>Character {id}</Text>
    </View>
  );
};
