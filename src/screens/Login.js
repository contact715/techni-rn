import { View, Button, TextInput, Text, StyleSheet } from "react-native";
import { useForm, Controller } from "react-hook-form";
import { routes } from "../constants/routes";

export default function Login({ navigation }) {
  const isHighlightedInput = false;

  const { control, handleSubmit } = useForm({
    defaultValues: {
      login: "somelogin",
      password: "somepassword",
    },
  });

  const onSubmit = (data) => {
    console.log(data);
    navigation.navigate(routes.DASHBOARD, { login: data.login });
  };

  return (
    <View style={styles.root}>
      <Controller
        control={control}
        name="login"
        rules={{
          required: "Field is required",
        }}
        render={({ field, fieldState }) => (
          <>
            <TextInput
              value={field.value}
              onChangeText={field.onChange}
              onBlur={field.onBlur}
              ref={field.ref}
              name={field.name}
              placeholder="Type your login..."
              style={styles.input}
            />
            {fieldState.error && <Text>{fieldState.error.message}</Text>}
          </>
        )}
      />
      <Controller
        control={control}
        name="password"
        rules={{
          required: true,
        }}
        render={({ field }) => (
          <TextInput
            value={field.value}
            onChangeText={field.onChange}
            onBlur={field.onBlur}
            ref={field.ref}
            name={field.name}
            secureTextEntry
            placeholder="Type your password..."
            style={[
              styles.input,
              isHighlightedInput ? styles.highlightedInput : {},
            ]}
          />
        )}
      />
      <Button title="Submit" onPress={handleSubmit(onSubmit)} />
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    gap: 8,
  },
  input: {
    borderColor: "black",
    borderWidth: 1,
    width: "80%",
    padding: 8,
    borderRadius: 8,
  },
  highlightedInput: {
    backgroundColor: "red",
  },
});
